This is a CNN based image classfier developed using Transfer Learning on the ResNet 50 model. To run the notebook, 
- Please download the dataset from https://www.kaggle.com/c/dog-breed-identification/data and place the extracted labels.csv, test and train folders containing images into the data folder.
- Make sure you have numpy, matplotlib, sklearn, jupyter and pytorch installed and working.
- Start a Notebook server by running `jupyter notebook` and open the notebook.